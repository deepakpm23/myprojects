package com.example.loaddatafromdb.service;

import java.util.ArrayList;
import java.util.Iterator;

import com.example.loaddatafromdb.model.Customer;
import com.example.loaddatafromdb.model.CustomerDataAccessObject;
import com.example.loaddatafromdb.model.DataAccessObject;

public class CustomerService {

	CustomerDataAccessObject accessObject;

	public CustomerService() {
		accessObject = new CustomerDataAccessObject(
				DataAccessObject.CUSTOMER_TABLE);

	}

	public long insetCustomer(Customer customer) {
		return accessObject.insetDB(customer);

	}

	public ArrayList<Customer> findAllCustomer() {
		ArrayList<Customer> custList = new ArrayList<Customer>();
		ArrayList<Object> objList = new ArrayList<Object>();
		objList = accessObject.findAll();
		for (Object obj : objList) {
			custList.add((Customer) obj);

		}
		return custList;

	}
}
