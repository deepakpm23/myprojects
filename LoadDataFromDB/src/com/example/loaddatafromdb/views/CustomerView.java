package com.example.loaddatafromdb.views;

import com.example.loaddatafromdb.R;
import com.example.loaddatafromdb.R.id;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

public class CustomerView extends LinearLayout {

	public CustomerView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public String getNameET() {
		return ((EditText) findViewById(R.id.name_et)).getText().toString();

	}

	public String getAddressET() {
		return ((EditText) findViewById(R.id.address_et)).getText().toString();
	}

	public String getPhoneET() {
		return ((EditText) findViewById(R.id.phno_et)).getText().toString();
	}

	public String getStateET() {
		return ((EditText) findViewById(R.id.state_et)).getText().toString();
	}

	public void onSaveButton(OnClickListener clickListener) {
		((Button) findViewById(R.id.save_btn))
				.setOnClickListener(clickListener);
	}

	public void setNameError(String error) {
		((EditText) findViewById(R.id.name_et)).setError(error);
	}

	public void setAddressError(String error) {
		((EditText) findViewById(R.id.address_et)).setError(error);
	}

	public void setPhoneError(String error) {
		((EditText) findViewById(R.id.phno_et)).setError(error);
	}

	public void setStateError(String error) {
		((EditText) findViewById(R.id.state_et)).setError(error);
	}

	public void onGetListener(OnClickListener clickListener) {
		((Button) findViewById(R.id.button1)).setOnClickListener(clickListener);
	}

}
