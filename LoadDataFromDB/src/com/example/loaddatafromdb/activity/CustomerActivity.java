package com.example.loaddatafromdb.activity;

import android.app.Activity;
import android.os.Bundle;

import com.example.loaddatafromdb.R;
import com.example.loaddatafromdb.controller.CustomerController;
import com.example.loaddatafromdb.views.CustomerView;

public class CustomerActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.customer_activity);
		CustomerController controller = new CustomerController(
				(CustomerView) findViewById(R.id.parent));
		((CustomerView) findViewById(R.id.parent)).onSaveButton(controller);
		((CustomerView) findViewById(R.id.parent)).onGetListener(controller);
	}

}
