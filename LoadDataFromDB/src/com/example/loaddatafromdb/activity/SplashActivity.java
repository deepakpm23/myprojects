package com.example.loaddatafromdb.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.example.loaddatafromdb.R;
import com.example.loaddatafromdb.controller.SplashController;
import com.example.loaddatafromdb.controller.TaskListner;

public class SplashActivity extends Activity implements TaskListner {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_activity);

		SplashController controller = new SplashController(this);
		controller.onSplash();
	}

	@Override
	public void onTaskFinish() {
		startActivity(new Intent(SplashActivity.this, CustomerActivity.class));
		finish();
	}

	@Override
	public void onTaskFail() {
		// TODO Auto-generated method stub

	}

}
