package com.example.loaddatafromdb.model;

import android.content.ContentValues;
import android.database.Cursor;

public class CustomerDataAccessObject extends DatatBaseDataAccessObjec {

	public CustomerDataAccessObject(String tableName) {
		super(tableName);
	}

	@Override
	public ContentValues objectToContentValue(Object object) {
		ContentValues contentValues = new ContentValues();
		Customer customer = new Customer();
		customer = (Customer) object;
		contentValues.put(CUSTOMER_NAME, customer.getCustomerName());
		contentValues.put(CUSTOMER_ADDRESS, customer.getCustomerAddress());
		contentValues.put(CUSTOMER_PHONE, customer.getCustomerPh());
		contentValues.put(CUSTOMER_STATE, customer.getCustomerState());

		return contentValues;
	}

	@Override
	public Object cursorToObject(Cursor cursor) {

		String id = cursor.getString(cursor.getColumnIndex(CUST_ID));
		String name = cursor.getString(cursor.getColumnIndex(CUSTOMER_NAME));
		String address = cursor.getString(cursor
				.getColumnIndex(CUSTOMER_ADDRESS));
		String phone = cursor.getString(cursor.getColumnIndex(CUSTOMER_PHONE));
		String state = cursor.getString(cursor.getColumnIndex(CUSTOMER_STATE));

		return new Customer(id, name, address, phone, state);
	}

}
