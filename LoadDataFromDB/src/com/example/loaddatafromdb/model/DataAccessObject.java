package com.example.loaddatafromdb.model;

import java.util.ArrayList;

public interface DataAccessObject {

	public String DBNAME = "Company.db";
	public int DB_VERSION = 1;

	/**
	 * Customer Table
	 */
	public String CUSTOMER_TABLE = "CustomerTable";
	/**
	 * User Table
	 */
	public String USER_TABLE = "UserTable";
	/**
	 * User Table Details
	 */
	public String USER_DETAILS_TABLE = "UserDetails";

	/**
	 * for User Table
	 * 
	 */
	public String USER_ID = "ID";
	public String USERID = "UserID";
	public String USERNAME = "UserName";
	public String PASSWORD = "Password";

	/**
	 * for User Details
	 */
	public String USER_D_ID = "ID";
	public String USER_NAME = "Name";
	public String USER_ADDRESS = "Address";
	public String USER_PHONE = "Phone";

	/**
	 * for Customer
	 */
	public String CUST_ID = "ID";
	public String CUSTOMER_NAME = "Name";
	public String CUSTOMER_PHONE = "Phone";
	public String CUSTOMER_STATE = "State";
	public String CUSTOMER_ADDRESS = "Address";

	/**
	 * Create Table For customer
	 */
	public String CREATE_CUSTOMER_TABLE = "CREATE TABLE " + CUSTOMER_TABLE
			+ "( " + CUST_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ CUSTOMER_NAME + " TEXT," + CUSTOMER_ADDRESS + " TEXT,"
			+ CUSTOMER_PHONE + " TEXT," + CUSTOMER_STATE + " TEXT)";

	public String CREATE_USER_DETAILS_TABLE = "CREATE TABLE "
			+ USER_DETAILS_TABLE + "( " + USER_D_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + USER_NAME + " TEXT,"
			+ USER_ADDRESS + " TEXT," + USER_PHONE + " TEXT)";

	/*public String CREATE_USER_TABLE = "CREATE TABLE " + USER_TABLE + "( "
			+ USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + USERNAME
			+ " TEXT," + PASSWORD + " TEXT," + USERID + " INTEGER,"
			+ " FOREIGN KEY (" + USERID + ") REFERENCES " + USER_DETAILS_TABLE
			+ " (" + USER_D_ID + "));)";*/
	
	public String CREATE_USER_TABLE = "CREATE TABLE " + USER_TABLE + "( "
			+ USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + USERNAME
			+ " TEXT," + PASSWORD + " TEXT," + USERID + " INTEGER)"
			+ " FOREIGN KEY (" + USERID + ") REFERENCES " + USER_DETAILS_TABLE
			+ " (" + USER_D_ID + ")";

	public String DROP_TABLE = "DROP TABLE IF EXISTS " + CUSTOMER_TABLE;
	public String SELECT_ALL_FROM_TABLE = "SELECT * FROM " + CUSTOMER_TABLE;

	public long insetDB(Object obj);

	ArrayList<Object> findAll();
}
