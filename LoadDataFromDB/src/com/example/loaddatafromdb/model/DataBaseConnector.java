package com.example.loaddatafromdb.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataBaseConnector extends SQLiteOpenHelper {
	private static String TAG = DataBaseConnector.class.getName();

	public DataBaseConnector(Context context) {
		super(context, DataAccessObject.DBNAME, null, DataAccessObject.DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DataAccessObject.CREATE_CUSTOMER_TABLE);
		db.execSQL(DataAccessObject.CREATE_USER_DETAILS_TABLE);
		db.execSQL(DataAccessObject.CREATE_USER_TABLE);
		Log.d(TAG, "Table Created");
		Log.d(TAG, DataAccessObject.CREATE_USER_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(DataAccessObject.CREATE_CUSTOMER_TABLE);
		db.execSQL(DataAccessObject.CREATE_USER_DETAILS_TABLE);
		db.execSQL(DataAccessObject.CREATE_USER_TABLE);
		onCreate(db);
		Log.d(TAG, "Table Upgraded");
	}

}
