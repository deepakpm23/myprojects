package com.example.loaddatafromdb.model;

import android.content.ContentValues;
import android.database.Cursor;

public class SignUpDataAccessObject extends DatatBaseDataAccessObjec {

	public SignUpDataAccessObject(String tableName) {
		super(tableName);
	}

	@Override
	public ContentValues objectToContentValue(Object object) {
		SignUpDetails details = new SignUpDetails();
		details = (SignUpDetails) object;

		ContentValues cv = new ContentValues();
		cv.put(USER_NAME, details.getName());
		cv.put(USER_ADDRESS, details.getAddress());
		cv.put(USER_PHONE, details.getPhone());

		return cv;
	}

	@Override
	public Object cursorToObject(Cursor cursor) {

		String name = cursor.getString(cursor.getColumnIndex(USER_NAME));
		String address = cursor.getString(cursor.getColumnIndex(USER_ADDRESS));
		String phone = cursor.getString(cursor.getColumnIndex(USER_PHONE));

		return new SignUpDetails(name, address, phone, "", "");

	}
}
