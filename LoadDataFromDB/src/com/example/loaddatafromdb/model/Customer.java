package com.example.loaddatafromdb.model;

public class Customer {
	private String id;
	private String CustomerName;
	private String CustomerAddress;
	private String CustomerPh;
	private String CustomerState;

	public Customer(String id, String customerName, String customerAddress,
			String customerPh, String customerState) {
		super();
		CustomerName = customerName;
		CustomerAddress = customerAddress;
		CustomerPh = customerPh;
		CustomerState = customerState;
		this.id = id;
	}

	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getCustomerName() {
		return CustomerName;
	}

	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	public String getCustomerAddress() {
		return CustomerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		CustomerAddress = customerAddress;
	}

	public String getCustomerPh() {
		return CustomerPh;
	}

	public void setCustomerPh(String customerPh) {
		CustomerPh = customerPh;
	}

	public String getCustomerState() {
		return CustomerState;
	}

	public void setCustomerState(String customerState) {
		CustomerState = customerState;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
