package com.example.loaddatafromdb.model;

import java.util.ArrayList;

import com.example.loaddatafromdb.helper.ApplicationHelper;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public abstract class DatatBaseDataAccessObjec implements DataAccessObject {

	DataBaseConnector baseConnector;
	String tablename;

	public DatatBaseDataAccessObjec(String tableName) {
		baseConnector = new DataBaseConnector(ApplicationHelper.getContext());
		this.tablename = tableName;
	}

	public String getTablename() {
		return tablename;
	}

	@Override
	public long insetDB(Object obj) {
		SQLiteDatabase db = baseConnector.getWritableDatabase();
		return db.insert(tablename, null, objectToContentValue(obj));
	}

	@Override
	public ArrayList<Object> findAll() {

		ArrayList<Object> objects = new ArrayList<Object>();

		SQLiteDatabase db = baseConnector.getReadableDatabase();

		Cursor cursor = db.rawQuery(DataAccessObject.SELECT_ALL_FROM_TABLE,
				null);
		if (cursor != null) {

			while (cursor.moveToNext()) {
				objects.add(cursorToObject(cursor));
			}
		}

		return objects;
	}

	public abstract ContentValues objectToContentValue(Object object);

	public abstract Object cursorToObject(Cursor cursor);

}
