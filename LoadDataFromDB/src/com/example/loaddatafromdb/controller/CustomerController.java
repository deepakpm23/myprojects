package com.example.loaddatafromdb.controller;

import java.util.ArrayList;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.example.loaddatafromdb.R;
import com.example.loaddatafromdb.helper.ApplicationHelper;
import com.example.loaddatafromdb.model.Customer;
import com.example.loaddatafromdb.model.CustomerDataAccessObject;
import com.example.loaddatafromdb.model.DataAccessObject;
import com.example.loaddatafromdb.model.DataBaseConnector;
import com.example.loaddatafromdb.service.CustomerService;
import com.example.loaddatafromdb.views.CustomerView;

public class CustomerController implements OnClickListener {
	CustomerView customerView;
	DataBaseConnector baseConnector;
	CustomerService cs;

	public CustomerController(CustomerView customerView) {
		this.customerView = customerView;
		cs = new CustomerService();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.save_btn:
			if (validate() != null) {

				Customer cus = new Customer(null, customerView.getNameET(),
						customerView.getAddressET(), customerView.getPhoneET(),
						customerView.getStateET());

				long id = cs.insetCustomer(cus);
				Toast.makeText(ApplicationHelper.getContext(),
						id + " Row Created", Toast.LENGTH_SHORT).show();

			}
			break;
		case R.id.button1:
			ArrayList<Customer> customersList = cs.findAllCustomer();

			for (Customer customer : customersList) {
				Toast.makeText(ApplicationHelper.getContext(),
						customer.getCustomerName(), Toast.LENGTH_SHORT).show();

			}
			break;

		default:
			break;
		}

	}

	public String validate() {
		StringBuilder message = new StringBuilder();
		if (customerView.getNameET().trim().toString().length() == 0) {
			customerView.setNameError("Please Fill Name");
			message.append("Error");
		} else if (customerView.getAddressET().trim().toString().length() == 0) {
			customerView.setAddressError("Please Fill Name");
			message.append("Error");
		} else if (customerView.getPhoneET().trim().toString().length() == 0) {
			customerView.setPhoneError("Please Fill Name");
			message.append("Error");
		} else if (customerView.getStateET().trim().toString().length() == 0) {
			customerView.setStateError("Please Fill Name");
			message.append("Error");
		}
		return message.toString();
	}
}
