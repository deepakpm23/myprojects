package com.example.loaddatafromdb.controller;

public interface TaskListner {

	void onTaskFinish();

	void onTaskFail();

}
