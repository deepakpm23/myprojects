package com.example.loaddatafromdb.controller;

import android.os.Handler;

public class SplashController {
	TaskListner listner;
	Handler handler;

	public SplashController(TaskListner listner) {
		this.listner = listner;

	}

	public void onSplash() {

		handler = new Handler();
		handler.postDelayed(new Runnable() {

			@Override
			public void run() {
				listner.onTaskFinish();
			}
		}, 1000);

	}
}
